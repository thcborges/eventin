Projeto Final IN Junior 2017.2

EventIN - Plataforma de eventos da IN Junior para eventos MEJ

Plataforma que vai administrar os eventos do MEJ brasileiro, com inscrições em palestras/workshop dentro dos eventos e alocação nas acomodações.

O sistema deve ter os seguintes detalhes abaixo. Você deverá pensar na melhor maneira de implementá-los e entender os detalhes implícitos.

Detalhes
	- O sistema deve ser capaz de exportar em uma planilha os usuários inscritos com o nome e o status deles no evento			
	- As inscrições para o evento só podem acontecer quando forem abertas dinamicamente por um horário escolhido pelo administrador, e fechadas quando programada ou quando acabar a quantidade disponível no lote.
	- O mesmo vale para a abertura de inscrições nas palestras/workshops
	- O sistema deverá contar com um ranking de número de inscritos no evento por EJ e por Federação
	- O sistema deve deixar prático para o usuário navegar no sistema e de forma ágil (UX)
	- As palestras/workshops podem ter materiais baixáveis
	- A inscrição do usuário deverá ser confirmada por email 
	- O usuário pode recuperar sua senha por email
	- O sistema deve conter versionamento diário funcional no GIT
	- O usuário pode fazer transferência de inscrição para outra pessoa (manter histórico de transferência)
	- Quando um usuário for deletado toda a informação que ele tinha (como inscrições nas palestras) deve sumir também.
	- O sistema deverá ter um countdown para mostrar quando abrirá as inscrições do evento, assim como para as palestras e para as acomodações.
	- As acomodações podem ter mais de um local (hotel/pousada/fazenda)


















