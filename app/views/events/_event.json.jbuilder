json.extract! event, :id, :begin, :end, :ctrl, :created_at, :updated_at
json.url event_url(event, format: :json)
