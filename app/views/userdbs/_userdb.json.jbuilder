json.extract! userdb, :id, :email, :name, :cpf, :tel, :ejid, :updatetype, :created_at, :updated_at
json.url userdb_url(userdb, format: :json)
