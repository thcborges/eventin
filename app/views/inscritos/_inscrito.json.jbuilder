json.extract! inscrito, :id, :user_id, :palestra_id, :created_at, :updated_at
json.url inscrito_url(inscrito, format: :json)
