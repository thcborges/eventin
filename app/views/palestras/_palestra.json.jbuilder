json.extract! palestra, :id, :title, :description, :vacancies, :palestra_file, :created_at, :updated_at
json.url palestra_url(palestra, format: :json)
