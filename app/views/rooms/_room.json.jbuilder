json.extract! room, :id, :accomm_id, :beds, :created_at, :updated_at
json.url room_url(room, format: :json)
