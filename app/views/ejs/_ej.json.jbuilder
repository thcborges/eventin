json.extract! ej, :id, :name, :activity, :federation_id, :city, :created_at, :updated_at
json.url ej_url(ej, format: :json)
