class Room < ActiveRecord::Base
  mount_uploader :room_photo, PictureUploader
  
  belongs_to :accomm
  has_many :users, dependent: :nullify
  
  validates :accomm_id, presence: true
  validates :beds, :inclusion => {:in => 1..4}
end