class Palestra < ActiveRecord::Base
    paginates_per 1
    
    mount_uploader :palestra_file, FileUploader
    
    
    has_many :pasers, dependent: :destroy
    has_many :inscritos, dependent: :destroy
    
    validates :title, length: {in: 3..63}, presence: true
    validates :description, length: {in: 0..255}, allow_nil: true
    validates :vacancies,  :inclusion => {:in => 20..300}
    
    
end