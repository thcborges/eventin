class User < ActiveRecord::Base
    mount_uploader :profile_photo, PictureUploader
    
     def self.to_csv
        users = User.all
        attributes = ["user_id" , "Usuario"]
    
        CSV.generate(headers: true) do |csv|
          csv << attributes
    
          all.each do |user|
            csv << [user.id, user.name]
          end
        end
     end
    
    attr_accessor :reset_token
    before_create :confirmation_token
    before_save :downcase_email
    
    has_secure_password
    
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    validates :email, length: {in: 10..50}, format: { with: VALID_EMAIL_REGEX }, uniqueness: true
    validates :cpf, uniqueness: true, presence: true, allow_nil: true
    validates :password, length: {in: 3..50, presence: true, allow_nil: true}
    
    belongs_to :ej
    belongs_to :room
    has_many :userdbs, dependent: :destroy
    has_many :pasers, dependent: :destroy
    has_many :inscritos, dependent: :destroy
     
     # Returns true if a password reset has expired.
    def password_reset_expired?
        reset_sent_at < 2.hours.ago
    end

     
     # Returns true if the given token matches the digest.
    def authenticated?(attribute, token)
        digest = send("#{attribute}_digest")
        return false if digest.nil?
        BCrypt::Password.new(digest).is_password?(token)
    end
     
    
    def email_activate
        self.email_confirmed = true
        self.confirm_token = nil
        save!(:validate => false)
    end
    
    def User.digest(string)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
    end
    # Returns a random token.
    def User.new_token
       SecureRandom.urlsafe_base64
    end
    
    #Downcade every email.
    def downcase_email
       self.email = email.downcase
    end
    
    # Métodos da recuperaçaõ de senha.
    def create_reset_digest
       self.reset_token = User.new_token
       update_attribute(:reset_digest,  User.digest(reset_token))
       update_attribute(:reset_sent_at, Time.zone.now)
    end
    
    def send_password_reset_email
       UserMailer.password_reset(self).deliver_now
    end
        
    private
     def confirmation_token
       if self.confirm_token.blank?
         self.confirm_token = SecureRandom.urlsafe_base64.to_s
       end
      end
end
