class Paser < ActiveRecord::Base
  belongs_to :user
  belongs_to :palestra
  
  def self.to_csv
    pasers = Paser.all
    attributes = ["user_id" , "usuario" , "palestra_id" , "palestra" , "confirmation"]

    CSV.generate(headers: true) do |csv|
      csv << attributes

      all.each do |paser|
        csv << [paser.user_id, paser.user.name,paser.palestra_id,paser.palestra.title,paser.confirmation]
      end
    end
  end

  def name
    "#{first_name} #{last_name}"
  end

end
