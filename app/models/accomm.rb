class Accomm < ActiveRecord::Base
    mount_uploader :accomm_photo, PictureUploader
    has_many :rooms, dependent: :destroy
    
    
    validates :accomm_photo, presence: true
    validates :name, presence: true
    validates :tipo, presence: true
    validates :adress, presence: true
end