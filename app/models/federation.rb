class Federation < ActiveRecord::Base
    mount_uploader :palestra_file, FileUploader
    has_many :ejs
    
    validates :name, length: {in: 2..50}
    
end
