class Ej < ActiveRecord::Base
  belongs_to :federation
  
  has_many :users, dependent: :nullify
  
  validates :name, uniqueness: true, length: {in: 2..50}
  validates :activity, length: {in: 5..140}
  
end
