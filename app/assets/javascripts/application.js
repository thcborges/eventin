// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require_tree .


function timer() {
    var dataAtual = new Date();
    var dataEvento = new Date('08/16/2017 20:00:00');
    var timer = dataEvento - dataAtual;
    var data = new Date(timer);
    var dias = data.getDate()
    var horas = "0" + data.getHours();
    var minutos = "0" + data.getMinutes();
    var segundos = "0" + data.getSeconds();
    document.getElementById('timer').innerHTML = "Faltam: " + dias + " dias, " + horas + ":" + minutos + ":" + segundos + " para abrirem as inscrições.";
    setTimeout('timer()', 500);
}

function abreMenu(checkbox) {
    var ofusca = document.getElementById('ofusca').classList;
    if (checkbox.checked == true) {
        ofusca.add('ofusca');
        ofusca.remove('desofusca');
    } else {
        ofusca.add('desofusca');
        ofusca.remove('ofusca');
    }
}

function fechaMenu() {
    document.getElementById('check').checked = false;
    abreMenu(document.getElementById('check'));
}

// $('#check').on('click', function () {
//     var condicao = $('#secao').hasClass('menu-aberto');
//     if (condicao) {
//         $('#secao').removeClass('menu-aberto');
//         $('#secao').addClass('menu-fechadomenu-aberto');
//     } else {
//         $('#secao').removeClass('menu-fechado');
//         $('#secao').addClass('menu-aberto');
//     }
// });

