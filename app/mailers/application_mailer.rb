class ApplicationMailer < ActionMailer::Base
  default from: "vitor.pivt@gmail.com"
  layout 'mailer'
end
