class UserSessionController < ApplicationController
  layout false
  before_action :session_params, only: [:create]
  before_action :logged_user, only: [:new, :create]
  before_action :time_ready, only: [:new]
  def new
  end
  def time_ready
        @event = Event.first
        if @event.begin < Date.new
            redirect_to home_path
            flash[:error] = "As inscrições ainda não abriram.."
        end
  end
    
  def create
    user = User.find_by(email: session_params[:email])
    if user && user.authenticate(session_params[:password])
    if user.email_confirmed
        log_in user
        flash[:success] = "Você foi autenticado com sucesso !"
        redirect_to user
    else
        flash.now[:error] = 'Please activate your account by following the 
        instructions in the account confirmation email you received to proceed'
        render 'new'
    end
    else
      render :new
    end
  end
  
  def destroy
    log_out
    redirect_to login_path
  end
  
  def session_params
    @session_params = params.require(:user_session).permit(:email, :password)
  end
end
