class PalestrasController < ApplicationController
  before_action :set_palestra, only: [:show, :edit, :update, :destroy, :verify_vacancies]
  

  before_action :require_user
  before_action :verify_vacancies, only: [:paser]
  
  
  def verify_vacancies
    @palestra = Palestra.find(params[:id])
    users = @palestra.pasers.count
    if users == @palestra.vacancies
      redirect_to palestras_path
    end
  end
  
  def require_palestrante
    if  !current_user.palestrante
      redirect_to users_path
    end
  end
  # GET /palestras
  # GET /palestras.json
  def index
    @palestras = Palestra.all

  end

  # GET /palestras/1
  # GET /palestras/1.json
  def show
    @paser = Paser.find_by(user_id: current_user.id, palestra_id: @palestra.id)
  end

  # GET /palestras/new
  
  def download_file
        @palestra = Palestra.find(params[:id])
        send_file(@palestra.palestra_file.path,
              :url_based_filename => false)
  end
  
  def new
    @palestra = Palestra.new
    @users = User.where(:palestrante => true)
  end

  def paser
    @paser = Paser.find_by(palestra_id: params[:id], user_id: current_user.id)
    if @paser.nil?
      Paser.create!(palestra_id: params[:id], user_id: current_user.id)
    else
      @paser.destroy
    end
    @paser = Paser.find_by(palestra_id: params[:id], user_id: current_user.id)
    palestra = Palestra.find(params[:id])
    respond_to do |format|
      format.html{redirect_to palestra}
      format.js {}
    end
  
  end
  
  
  # GET /palestras/1/edit
  def edit
    @users = User.where(:palestrante => true)
  end

  # POST /palestras
  # POST /palestras.json
  def create
    @palestra = Palestra.new(palestra_params)

    respond_to do |format|
      if @palestra.save
        format.html { redirect_to @palestra, notice: 'Palestra was successfully created.' }
        format.json { render :show, status: :created, location: @palestra }
      else
        format.html { render :new }
        format.json { render json: @palestra.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /palestras/1
  # PATCH/PUT /palestras/1.json
  def update
    respond_to do |format|
      if @palestra.update(palestra_params)
        format.html { redirect_to @palestra, notice: 'Palestra was successfully updated.' }
        format.json { render :show, status: :ok, location: @palestra }
      else
        format.html { render :edit }
        format.json { render json: @palestra.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /palestras/1
  # DELETE /palestras/1.json
  def destroy
    @palestra.destroy
    respond_to do |format|
      format.html { redirect_to palestras_url, notice: 'Palestra was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_palestra
      @palestra = Palestra.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def palestra_params
      params.require(:palestra).permit(:title, :description, :vacancies, :palestra_file,:speaker,:dat)
    end
end
