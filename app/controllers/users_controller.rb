class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :logged_user, only: [:new, :create]
  before_action :is_admin, except: [:show,:email_activate,:confirm_email]
  before_action :require_user
 
  
  
  
  def crud
    @users = User.paginate(:page => params[:page], :per_page => 10)
  end
  
  def email_activate
        self.email_confirmed = true
        self.confirm_token = nil
        save!(:validate => false)
  end
  def confirm_email
      user = User.find_by_confirm_token(params[:id])
      if user
        user.email_activate
        flash[:success] = "Bem-vindo, seu e-mail foi ativado com sucesso!"
        redirect_to login_path
      else
        flash[:error] = "Sorry. User does not exist"
        redirect_to root_url
      end
  end
  
  # GET /users
  # GET /users.json
  def index
    @users = User.all
    respond_to do |format|
        format.html
        format.csv { send_data @users.to_csv, filename: "users-#{Date.today}.csv" }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @ejs = Ej.all
    @friends = User.where(:room_id => current_user.room_id)
    @friends = @friends.where.not(:id => current_user.id)
  end

  # GET /users/new
  def new
    @user = User.new
    @ej = Ej.first
  end

  # GET /users/1/edit
  def edit
  end
  

  # POST /users
  # POST /users.json
  def create
     @user = User.new(user_params)
      if @user.save
        UserMailer.registration_confirmation(@user).deliver
        flash[:success] = "Please confirm your email address to continue"
        redirect_to root_path
      else
        respond_to do |format|
          format.html { render :new }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if !@user.ej_id.nil?
      ejt = Ej.find(@user.ej_id)
      if !ejt.nil?
        ejt.counter = ejt.users.count
        ejt.save
      end
    end
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to crud_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :name, :cpf, :cep, :tel, :ej_id, :profile_photo)
    end
end
