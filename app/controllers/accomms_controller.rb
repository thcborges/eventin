class AccommsController < ApplicationController
  before_action :set_accomm, only: [:show, :edit, :update, :destroy, :index_rooms, :especific_rooms]
  before_action :is_admin, except: [:show,:index,:index_rooms,:especific_rooms]

  # GET /accomms
  # GET /accomms.json
  def index
    @accomms = Accomm.all
  end
  
  def index_rooms
    @rooms = @accomm.rooms
  end
  
  def especific_rooms
    @rooms = Room.where(:beds => params[:beds])
  end
  # GET /accomms/1
  # GET /accomms/1.json
  def show
  end

  # GET /accomms/new
  def new
    @accomm = Accomm.new
  end

  # GET /accomms/1/edit
  def edit
  end

  # POST /accomms
  # POST /accomms.json
  def create
    @accomm = Accomm.new(accomm_params)

    respond_to do |format|
      if @accomm.save
        format.html { redirect_to @accomm, notice: 'Accomm was successfully created.' }
        format.json { render :show, status: :created, location: @accomm }
      else
        format.html { render :new }
        format.json { render json: @accomm.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accomms/1
  # PATCH/PUT /accomms/1.json
  def update
    respond_to do |format|
      if @accomm.update(accomm_params)
        format.html { redirect_to @accomm, notice: 'Accomm was successfully updated.' }
        format.json { render :show, status: :ok, location: @accomm }
      else
        format.html { render :edit }
        format.json { render json: @accomm.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accomms/1
  # DELETE /accomms/1.json
  def destroy
    @accomm.destroy
    respond_to do |format|
      format.html { redirect_to accomms_url, notice: 'Accomm was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_accomm
      @accomm = Accomm.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def accomm_params
      params.require(:accomm).permit(:adress, :tipo, :name,:accomm_photo)
    end
end
