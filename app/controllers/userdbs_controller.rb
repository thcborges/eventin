class UserdbsController < ApplicationController
  before_action :set_userdb, only: [:show, :edit, :update, :destroy]
  before_action :is_admin, only: :index
  # GET /userdbs
  # GET /userdbs.json
  def index
    @userdbs = Userdb.paginate(:page => params[:page], :per_page => 10)
  end

  # GET /userdbs/1
  # GET /userdbs/1.json
  def show
    redirect_to edit_userdb_path
  end

  # GET /userdbs/new
  def new
    @userdb = Userdb.last
  end

  # GET /userdbs/1/edit
  def edit
    @user = current_user
    if @userdb.updatetype == 1
      @user.email = @userdb.email
      @user.name = @userdb.name
      @user.cpf = @userdb.cpf
      @user.ej_id = @userdb.ejid
      @user.save
    else
      if @userdb.updatetype == 2
        @receptor = User.find_by(cpf: @userdb.cpf)
        if @user.payment != true
          flash[:error] = "Pagamento ainda não realizado"
        else
          if @receptor.payment ==true
            flash[:error] = "O usuário referido já realizou o pagamento"
          else
            @receptor.payment = true
            @user.payment = false
            @receptor.save
            @user.save
          end
        end
      end
    end
  end

  def transfer_type
    @userdb = Userdb.new
    @userdb.conf = true
    @userdb.save
  end
  # POST /userdbs
  # POST /userdbs.json
  def create
    @userdb = Userdb.new(userdb_params)
    @userdb.conf = true
    @user = current_user
    if @userdb.updatetype == 1
      @user.email = @userdb.email
      @user.name = @userdb.name
      @user.cpf = @userdb.cpf
      @user.ej_id = @userdb.ejid
      @user.save
    else
      if @userdb.updatetype == 2
        @receptor = User.find_by(cpf: @userdb.cpf)
        if @user.payment != true
          flash[:error] = "Pagamento ainda não realizado"
        else
          if @receptor.payment ==true
            flash[:error] = "O usuário referido já realizou o pagamento"
          else
            @receptor.payment = true
            @user.payment = false
            @receptor.save
            @user.save
          end
        end
      end
    end
    respond_to do |format|
      if @userdb.save
        format.html { redirect_to new_userdb_path, notice: 'Userdb was successfully created.' }
        format.json { render :show, status: :created, location: @userdb }
      else
        format.html { render :new }
        format.json { render json: @userdb.errors, status: :unprocessable_entity }
      end
    end
  end
  # PATCH/PUT /userdbs/1
  # PATCH/PUT /userdbs/1.json
  def update
    @user = current_user
    if @userdb.updatetype == 1
      @user.email = @userdb.email
      @user.name = @userdb.name
      @user.cpf = @userdb.cpf
      @user.ej_id = @userdb.ejid
      @user.save
    else
      if @userdb == 2
        @receptor = User.find_by(cpf: @userdb.cpf)
        if @user.payment != true
          flash[:error] = "Pagamento ainda não realizado"
        else
          if @receptor.payment ==true
            flash[:error] = "O usuário referido já realizou o pagamento"
          else
            @receptor.payment = true
            @user.payment = false
            @receptor.save
            @user.save
          end
        end
      end
    end
    respond_to do |format|
      if @userdb.update(userdb_params)
        format.html { redirect_to @userdb, notice: 'Userdb was successfully updated.' }
        format.json { render :show, status: :ok, location: @userdb }
      else
        format.html { render :edit }
        format.json { render json: @userdb.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /userdbs/1
  # DELETE /userdbs/1.json
  def destroy
    @userdb.destroy
    respond_to do |format|
      format.html { redirect_to userdbs_url, notice: 'Userdb was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_userdb
      @userdb = Userdb.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def userdb_params
      params.require(:userdb).permit(:email, :name, :cpf, :tel, :ejid, :updatetype)
    end
end