class PasersController < ApplicationController
  before_action :set_paser, only: [:show, :edit, :update, :destroy]
  before_action :is_admin, except: [:index]

  # GET /pasers
  # GET /pasers.json
  def index
    @pasers = Paser.all

      respond_to do |format|
        format.html
        format.csv { send_data @pasers.to_csv, filename: "pasers-#{Date.today}.csv", :disposition => 'inline' }
  
      end
  end

  # GET /pasers/1
  # GET /pasers/1.json
  def show
  end

  # GET /pasers/new
  def new
    @paser = Paser.new
  end

  # GET /pasers/1/edit
  def edit
  end

  # POST /pasers
  # POST /pasers.json
  def create
    @paser = Paser.new(paser_params)

    respond_to do |format|
      if @paser.save
        format.html { redirect_to @paser, notice: 'Paser was successfully created.' }
        format.json { render :show, status: :created, location: @paser }
      else
        format.html { render :new }
        format.json { render json: @paser.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pasers/1
  # PATCH/PUT /pasers/1.json
  def update
    respond_to do |format|
      if @paser.update(paser_params)
        format.html { redirect_to @paser, notice: 'Paser was successfully updated.' }
        format.json { render :show, status: :ok, location: @paser }
      else
        format.html { render :edit }
        format.json { render json: @paser.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pasers/1
  # DELETE /pasers/1.json
  def destroy
    @paser.destroy
    respond_to do |format|
      format.html { redirect_to pasers_url, notice: 'Paser was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_paser
      @paser = Paser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def paser_params
      params.require(:paser).permit(:user_id, :palestra_id)
    end
end
