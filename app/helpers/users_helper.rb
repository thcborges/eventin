module UsersHelper
    def is_admin
        if !current_user.admin
            redirect_to login_path
        end
    end
    
    def correct_user
        if current_user != @user
            redirect_to login_path
        end
    end
end
