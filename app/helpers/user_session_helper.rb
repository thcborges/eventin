module UserSessionHelper
    
  def log_in(user)
    session[:user_id] = user.id
  end
  
   # Se houver alguem logado, retornara o usuario
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  # Se houver alguem logado, retorna true
  def logged_in?
    !current_user.nil?
  end
  
  def correct_user_or_admin
      if current_user != @user && !current_user.admin
          redirect_to user_path(current_user)
      end
  end
  
  # Retira o id do usuario logado do cookie de sessao e limpa o current user
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end
  
  # Redirecionar para o perfil do Uusario, caso o mesmo já esteja logado
  def logged_user
    if logged_in?
      redirect_to user_path(current_user)
    end
  end
    
  def require_user
    if !logged_in?
      redirect_to login_path
    end
  end
end
