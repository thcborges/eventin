require 'test_helper'

class UserdbsControllerTest < ActionController::TestCase
  setup do
    @userdb = userdbs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:userdbs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create userdb" do
    assert_difference('Userdb.count') do
      post :create, userdb: { cpf: @userdb.cpf, ejid: @userdb.ejid, email: @userdb.email, name: @userdb.name, tel: @userdb.tel, updatetype: @userdb.updatetype }
    end

    assert_redirected_to userdb_path(assigns(:userdb))
  end

  test "should show userdb" do
    get :show, id: @userdb
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @userdb
    assert_response :success
  end

  test "should update userdb" do
    patch :update, id: @userdb, userdb: { cpf: @userdb.cpf, ejid: @userdb.ejid, email: @userdb.email, name: @userdb.name, tel: @userdb.tel, updatetype: @userdb.updatetype }
    assert_redirected_to userdb_path(assigns(:userdb))
  end

  test "should destroy userdb" do
    assert_difference('Userdb.count', -1) do
      delete :destroy, id: @userdb
    end

    assert_redirected_to userdbs_path
  end
end
