require 'test_helper'

class PasersControllerTest < ActionController::TestCase
  setup do
    @paser = pasers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pasers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create paser" do
    assert_difference('Paser.count') do
      post :create, paser: { palestra_id: @paser.palestra_id, user_id: @paser.user_id }
    end

    assert_redirected_to paser_path(assigns(:paser))
  end

  test "should show paser" do
    get :show, id: @paser
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @paser
    assert_response :success
  end

  test "should update paser" do
    patch :update, id: @paser, paser: { palestra_id: @paser.palestra_id, user_id: @paser.user_id }
    assert_redirected_to paser_path(assigns(:paser))
  end

  test "should destroy paser" do
    assert_difference('Paser.count', -1) do
      delete :destroy, id: @paser
    end

    assert_redirected_to pasers_path
  end
end
