require 'test_helper'

class EjsControllerTest < ActionController::TestCase
  setup do
    @ej = ejs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ejs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ej" do
    assert_difference('Ej.count') do
      post :create, ej: { activity: @ej.activity, city: @ej.city, federation_id: @ej.federation_id, name: @ej.name }
    end

    assert_redirected_to ej_path(assigns(:ej))
  end

  test "should show ej" do
    get :show, id: @ej
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ej
    assert_response :success
  end

  test "should update ej" do
    patch :update, id: @ej, ej: { activity: @ej.activity, city: @ej.city, federation_id: @ej.federation_id, name: @ej.name }
    assert_redirected_to ej_path(assigns(:ej))
  end

  test "should destroy ej" do
    assert_difference('Ej.count', -1) do
      delete :destroy, id: @ej
    end

    assert_redirected_to ejs_path
  end
end
