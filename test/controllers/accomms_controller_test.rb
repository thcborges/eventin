require 'test_helper'

class AccommsControllerTest < ActionController::TestCase
  setup do
    @accomm = accomms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:accomms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create accomm" do
    assert_difference('Accomm.count') do
      post :create, accomm: { adress: @accomm.adress, type: @accomm.type }
    end

    assert_redirected_to accomm_path(assigns(:accomm))
  end

  test "should show accomm" do
    get :show, id: @accomm
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @accomm
    assert_response :success
  end

  test "should update accomm" do
    patch :update, id: @accomm, accomm: { adress: @accomm.adress, type: @accomm.type }
    assert_redirected_to accomm_path(assigns(:accomm))
  end

  test "should destroy accomm" do
    assert_difference('Accomm.count', -1) do
      delete :destroy, id: @accomm
    end

    assert_redirected_to accomms_path
  end
end
