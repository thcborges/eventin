Rails.application.routes.draw do
  
  #Rota crud
  get 'users/crud', to: 'users#crud', as: :crud
  
  #Rotas do scaffold
  
  resources :inscritos
  resources :events
  resources :rooms
  resources :accomms
  resources :rooms
  resources :federations
  resources :ejs
  resources :pasers
  resources :palestras
  resources :password_resets, only: [:new, :create, :edit, :update]

  
  #Rotas para a home page
  
  get 'static_pages/home', as: :home
  root 'static_pages#home'
  
  #Rota das acomodações
  
  get 'accomms/:id/rooms', to: 'accomms#index_rooms', as: :index_rooms
  
  #Rota para busca de quartos com camas especificas
  
  get 'accomms/:id/rooms/:beds', to: 'accomms#especific_rooms', as: :especific_rooms

  #Rotas do user_controller
  
  
  
  #Rota para as transferências
  
  get 'userdbs/transferencia', to: 'userdbs#transfer_type', as: :transfer_type
  post 'userdbs/transferencia', to: 'userdbs#create', as: :transfer_type_post
  
  #Rota de relação palestra-usuario
  
  post 'palestras/:id/entrar', to: 'palestras#paser', as: :pasers_new2
  
  
  # Rota para a inscrição dos usuários nos quartos
  post 'rooms/:id/entrar', to: 'rooms#insc_room', as: :insc_room
 
  # Rota de confirmação
  get 'alteration', to: 'userdbs#alteration', as: :alteration
  
  
  #Rota para download de arquivos da palestra
  
  get '/download_file', to: 'palestras#download_file', as: :download_file
  
  # Rotas do user_session_controller
  
  get '/login', to: 'user_session#new', as: :login
  post '/login', to: 'user_session#create'
  delete '/logout', to: 'user_session#destroy', as: :logout

  resources :users do
    member do
      get :confirm_email
    end
  end
  resources :userdbs
end