class AddStartingToEvents < ActiveRecord::Migration
  def change
    add_column :events, :starting, :datetime
  end
end
