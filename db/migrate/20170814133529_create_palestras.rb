class CreatePalestras < ActiveRecord::Migration
  def change
    create_table :palestras do |t|
      t.string :title
      t.string :description
      t.integer :vacancies
      t.string :palestra_file

      t.timestamps null: false
    end
  end
end
