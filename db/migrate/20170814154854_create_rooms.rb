class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.references :accomm, index: true, foreign_key: true
      t.integer :beds

      t.timestamps null: false
    end
  end
end
