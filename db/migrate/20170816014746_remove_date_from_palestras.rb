class RemoveDateFromPalestras < ActiveRecord::Migration
  def change
    remove_column :palestras, :date, :string
    remove_column :palestras, :hour, :string
  end
end