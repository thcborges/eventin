class AddSpeakerToPalestra < ActiveRecord::Migration
  def change
    add_column :palestras, :speaker, :string
  end
end
