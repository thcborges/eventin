class AddUseridToUserdbs < ActiveRecord::Migration
  def change
    add_reference :userdbs, :user, index: true, foreign_key: true
  end
end
