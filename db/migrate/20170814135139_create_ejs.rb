class CreateEjs < ActiveRecord::Migration
  def change
    create_table :ejs do |t|
      t.string :name
      t.text :activity
      t.references :federation, index: true, foreign_key: true
      t.string :city
      t.integer :counter
      
      t.timestamps null: false
    end
  end
end
