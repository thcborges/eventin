class CreateAccomms < ActiveRecord::Migration
  def change
    create_table :accomms do |t|
      t.string :adress
      t.string :tipo

      t.timestamps null: false
    end
  end
end
