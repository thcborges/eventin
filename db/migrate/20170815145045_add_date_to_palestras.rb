class AddDateToPalestras < ActiveRecord::Migration
  def change
    add_column :palestras, :date, :string
    add_column :palestras, :hour, :string
  end
end
