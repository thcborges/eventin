class CreateInscritos < ActiveRecord::Migration
  def change
    create_table :inscritos do |t|
      t.references :user, index: true, foreign_key: true
      t.references :palestra, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
