class AddRoomPhotoToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :room_photo, :string
  end
end
