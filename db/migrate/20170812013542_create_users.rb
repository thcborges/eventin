class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :name
      t.string :cpf
      t.string :cep
      t.string :tel
      t.references :ej, foreign_key: true
      t.references :room, foreign_key: true
      t.boolean :palestrante
      t.boolean :admin
      t.string :password_digest

      t.timestamps null: false
    end
    add_index :users, :email
  end
end
