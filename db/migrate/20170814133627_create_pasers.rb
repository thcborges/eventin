class CreatePasers < ActiveRecord::Migration
  def change
    create_table :pasers do |t|
      t.references :user, index: true, foreign_key: true
      t.references :palestra, index: true, foreign_key: true
      t.boolean "confirmation"

      t.timestamps null: false
    end
  end
end
