class CreateUserdbs < ActiveRecord::Migration
  def change
    create_table :userdbs do |t|
      t.string :email
      t.string :name
      t.string :cpf
      t.string :tel
      t.integer :ejid
      t.integer :updatetype

      t.timestamps null: false
    end
  end
end
