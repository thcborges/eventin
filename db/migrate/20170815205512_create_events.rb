class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.datetime :begin
      t.datetime :end
      t.boolean :ctrl

      t.timestamps null: false
    end
  end
end
