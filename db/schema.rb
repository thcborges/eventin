# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170817015403) do

  create_table "accomms", force: :cascade do |t|
    t.string   "adress"
    t.string   "tipo"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "name"
    t.string   "accomm_photo"
  end

  create_table "ejs", force: :cascade do |t|
    t.string   "name"
    t.text     "activity"
    t.integer  "federation_id"
    t.string   "city"
    t.integer  "counter"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "ejs", ["federation_id"], name: "index_ejs_on_federation_id"

  create_table "events", force: :cascade do |t|
    t.datetime "begin"
    t.datetime "end"
    t.boolean  "ctrl"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "starting"
  end

  create_table "federations", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inscritos", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "palestra_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "inscritos", ["palestra_id"], name: "index_inscritos_on_palestra_id"
  add_index "inscritos", ["user_id"], name: "index_inscritos_on_user_id"

  create_table "palestras", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "vacancies"
    t.string   "palestra_file"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "speaker"
    t.datetime "dat"
  end

  create_table "pasers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "palestra_id"
    t.boolean  "confirmation"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "pasers", ["palestra_id"], name: "index_pasers_on_palestra_id"
  add_index "pasers", ["user_id"], name: "index_pasers_on_user_id"

  create_table "rooms", force: :cascade do |t|
    t.integer  "accomm_id"
    t.integer  "beds"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "room_photo"
  end

  add_index "rooms", ["accomm_id"], name: "index_rooms_on_accomm_id"

  create_table "userdbs", force: :cascade do |t|
    t.string   "email"
    t.string   "name"
    t.string   "cpf"
    t.string   "tel"
    t.integer  "ejid"
    t.integer  "updatetype"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "conf"
    t.integer  "user_id"
  end

  add_index "userdbs", ["user_id"], name: "index_userdbs_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "name"
    t.string   "cpf"
    t.string   "cep"
    t.string   "tel"
    t.integer  "ej_id"
    t.integer  "room_id"
    t.boolean  "palestrante"
    t.boolean  "admin"
    t.string   "password_digest"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.boolean  "email_confirmed", default: false
    t.string   "confirm_token"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.string   "profile_photo"
    t.boolean  "payment"
  end

  add_index "users", ["email"], name: "index_users_on_email"

end
